local cmd = vim.cmd -- execute vim commands
local ag = vim.api.nvim_create_augroup -- create autogroup
local au = vim.api.nvim_create_autocmd -- create autocommand

-- don't autocommenting new lines
cmd([[au BufEnter * set fo-=c fo-=r fo-=o]])

-- highlight yanked text
au("TextYankPost", {
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- show cursor line only in active window
local cursorGrp = ag("CursorLine", { clear = true })
au({ "InsertLeave", "WinEnter" }, {
  pattern = "*",
  command = "set cursorline",
  group = cursorGrp,
})
au({ "InsertEnter", "WinLeave" }, { pattern = "*", command = "set nocursorline", group = cursorGrp })

-- Enable spell checking for certain file types
au({ "BufRead", "BufNewFile" }, {
  pattern = { "*.txt", "*.md" },
  callback = function()
    vim.opt_local.spell = true
    vim.opt_local.spelllang = "en,es"
  end,
})

-- go to last loc when opening a buffer
local LastLoc = ag("last_loc", { clear = true })
au("BufReadPost", {
  group = LastLoc,
  callback = function(event)
    local exclude = { "gitcommit" }
    local buf = event.buf
    if vim.tbl_contains(exclude, vim.bo[buf].filetype) then
      return
    end
    local mark = vim.api.nvim_buf_get_mark(buf, '"')
    local lcount = vim.api.nvim_buf_line_count(buf)
    if mark[1] > 0 and mark[1] <= lcount then
      pcall(vim.api.nvim_win_set_cursor, 0, mark)
    end
  end,
})
