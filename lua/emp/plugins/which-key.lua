return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 500
  end,
  opts = {
    -- yoru configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
    groups = {
      mode = { "n", "v" },
      ["<leader>b"] = { name = "Buffers" },
      ["<leader>c"] = { name = "Code" },
      ["<leader>e"] = { name = "Explorer" },
      ["<leader>f"] = { name = "Find" },
      ["<leader>r"] = { name = "Lsp" },
      ["<leader>s"] = { name = "Splits" },
      ["<leader>t"] = { name = "Tabs" },
      ["<leader>w"] = { name = "Workspace" },
    },
  },
  config = function(_, opts)
    local wk = require("which-key")
    wk.setup(opts)
    wk.register(opts.groups)
  end,
}
